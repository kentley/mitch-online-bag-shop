                                                                       PHONE CSS RESPONSIVENESS

Reference:
https://css-tricks.com/snippets/css/media-queries-for-standard-devices/
https://www.w3schools.com/css/tryit.asp?filename=tryresponsive_breakpoints

/* ----------- iPhone 6, 6S, 7 and 8 ----------- */

/* Portrait and Landscape */
@media only screen 
  and (min-device-width: 375px) 
  and (max-device-width: 667px) 
  and (-webkit-min-device-pixel-ratio: 2) { 
   
    Insert code here...

}

/* 
   Note: If you want to use separate style for Phone Portrait and Phone Landscape.
   Just uncomment the Portrait CSS script and Landscapr CSS script.
*/

/* Portrait */
/* @media only screen 
  and (min-device-width: 375px) 
  and (max-device-width: 667px) 
  and (-webkit-min-device-pixel-ratio: 2)
  and (orientation: portrait) { 

    Insert code here...
    
} */

/* Landscape */
/* @media only screen 
  and (min-device-width: 375px) 
  and (max-device-width: 667px) 
  and (-webkit-min-device-pixel-ratio: 2)
  and (orientation: landscape) { 

    Insert code here...
 
} */


/* ----------- iPhone 6+, 7+ and 8+ ----------- */

/* Portrait and Landscape */
@media only screen 
  and (min-device-width: 414px) 
  and (max-device-width: 736px) 
  and (-webkit-min-device-pixel-ratio: 3) { 

    Insert code here...

}

/* 
   Note: If you want to use separate style for Phone Portrait and Phone Landscape.
   Just uncomment the Portrait CSS script and Landscapr CSS script.
*/

/* Portrait */
/* @media only screen 
  and (min-device-width: 414px) 
  and (max-device-width: 736px) 
  and (-webkit-min-device-pixel-ratio: 3)
  and (orientation: portrait) { 

    Insert code here...
 
} */

/* Landscape */
/* @media only screen 
  and (min-device-width: 414px) 
  and (max-device-width: 736px) 
  and (-webkit-min-device-pixel-ratio: 3)
  and (orientation: landscape) { 

    Insert code here...
 
} */